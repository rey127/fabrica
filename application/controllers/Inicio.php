<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function index()
	{
		$this->load->view('figuras', array(
																					"circulo" => $this->fabrica->crearFigura("Circulo",4.15),
																					"triangulo" => $this->fabrica->crearFigura("Triangulo",null, 7, 8),
																					"cuadro" => $this->fabrica->crearFigura("Cuadro",null, 10, 10)
																		 ));
	}
}

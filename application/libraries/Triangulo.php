<?php
require_once ('FiguraGeometrica.php');
class Triangulo extends FiguraGeometrica{
  public $superficie;
  public function __construct($base, $altura) {
    parent::__construct("Triangulo", $base, $altura);
    $this->superficie = $this->getSuperficie();
  }

  public function getSuperficie(){
    return (($this->getBase() * $this->getAltura()) / 2);
  }

}

<?php
require_once ('FiguraGeometrica.php');
class Cuadrado extends FiguraGeometrica{
  public $superficie;
  public function __construct($base, $altura) {
    parent::__construct("Cuadrado", $base, $altura);
    $this->superficie = $this->getSuperficie();
  }

  public function getSuperficie(){
      return pow($this->getBase(), 2);
  }

}

<?php
require_once ('FiguraGeometrica.php');
class Circulo extends FiguraGeometrica{
  private $pi;
  public $superficie;

  function __construct($diametro) {
    parent::__construct("Circulo", null, null, $diametro);
    $this->pi = 3.14;
    $this->superficie = $this->getSuperficie();
  }

  function getSuperficie(){
    return ($this->pi * pow(($this->getDiametro()/2), 2));
  }

}

<?php 

public class Triangulo extends FiguraGeometrica{

  public function __construct($base, $altura) {
    parent::__construct("Triangulo", $base, $altura);
  }

  public function getSuperficie(){
    return (($this->getBase() * $this->getHeight()) / 2);
  }

}

<?php
require_once ('FiguraGeometrica.php');
class Circulo extends FiguraGeometrica{
  private $pi;

  function __construct($diametro) {
    parent::__construct("Circulo", null, null, $diametro);
    $this->pi = 3.14;
  }

  function getSuperficie(){
    return ($this->pi * pow(($this->getDiametro()/2), 2));
  }

}

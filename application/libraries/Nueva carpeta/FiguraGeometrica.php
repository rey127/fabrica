<?php
abstract class FiguraGeometrica{
    private $tipoFigura;
    private $altura;
    private $base;
    private $diametro;

    public function __construct($tipoFigura, $altura = null, $base=null, $diametro = null) {
        $this->tipoFigura = $tipoFigura;
        $this->altura = $altura;
        $this->base = $base;
        $this->diametro = $diametro;
    }

    public function getAltura(){
      return $this->altura;
    }

    public function getBase(){
      return $this->base;
    }

    public function getDiametro(){
      return $this->diametro;
    }  

}

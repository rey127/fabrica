<?php namespace appFabrica;

public class Cuadrado extends FiguraGeometrica{

  public function __construct($base, $altura) {
    parent::__construct("Cuadrado", $base, $altura);
  }

  public function getSuperficie(){
      return pow($this->getBase(), 2);
  }

}

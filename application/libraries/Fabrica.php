<?php
require_once ('Circulo.php');
require_once ('Cuadrado.php');
require_once ('Triangulo.php');

class Fabrica{

  function __construct(){}

  function crearFigura($figura = null, $diametro = null, $base = null, $altura = null){
    switch ($figura) {
      case 'Cuadro':
        return new Cuadrado($base, $altura);
        break;
      case 'Triangulo':
        return new Triangulo($base, $altura);
        break;
      case 'Circulo':
          return new Circulo($diametro);
        break;
      default:
        return null;
        break;
    }
  }

}

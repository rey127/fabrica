<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet"href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">
  </head>
  <body>
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-xs-offset-2" style="border:1px #CCC solid; padding:10px;">
              <h3>Figuras Geométricas <small>(Circulo, Cuadro, Triangulo)</small></h3>
              <hr>

              <h4 class="text-danger">Circulo</h4>
              <small>Diametro: <strong><?php echo $circulo->diametro; ?></strong></small><br>
              <small>Superficie: <strong><?php echo $circulo->superficie; ?></strong></small>
              <br>
              <h4 class="text-success">Triángulo</h4>
              <small>Base: <strong><?php echo $triangulo->base; ?></strong></small><br>
              <small>Altura: <strong><?php echo $triangulo->altura; ?></strong></small><br>
              <small>Superficie: <strong><?php echo $triangulo->superficie; ?></strong></small>
              <br>
              <h4 class="text-primary">Cuadro</h4>
              <small>Base: <strong><?php echo $cuadro->base; ?></strong></small><br>
              <small>Altura: <strong><?php echo $cuadro->altura; ?></strong></small><br>
              <small>Superficie: <strong><?php echo $cuadro->superficie; ?></strong></small>
          </div>
        </div>
      </div>
  </body>
</html>
